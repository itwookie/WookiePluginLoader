Wookie Plugin Loader
=====

A quick and easy way to load plugins in Java
-----

All you need to do to write a plugin is to annotate any class in you jar file with the WookiePluginAnnotation

```java
@WookiePluginAnnotation(
        Name="Your Plugin Name", 
        Author="IT-Wookie", 
        Build = 1,
        Description="Plugin for demonstration",
        Version="1.0")
public class MyPlugin {...
```

Build is a version id for simpler compatibility checks than parsing the string version. Name, Author and Description are Strings.

Your plugin may then provide 3 methods annotated with WookiePluginFunction. These functions will be called in certain points to give the plugin an entry point into the host API.

`@WookiePluginFunction(Type)`

There are three types of function:

- `OnPrepare` is called as soon as the plugin class was found and scanned for methods
- `OnLoaded` is called once the scan for the jar file has finished and all PluginContainers were created
- `OnUnload` is just a signal for the plugin to close all streams and running tasks as the application is about to shut down

PluginContainers provide annotated information like Author at runtime as well as the File the plugin was loaded from and the PluginLoader the plugin and container belongs to.

The parent API can load plugins with only a few lines of code:

```java
//Load plugins
PluginLoader pl = new PluginLoader();
List<PluginContainers> pluginsInJar = pl.loadJar(new File("TestPlugin.jar"), true);
System.out.println("Loaded " + pluginsInJar.size() + " plugins");

//Do something with your plugins
//get all plugin as all are subclass of the template
System.out.println("There's now a total of " + pl.getPluginsBySuperclass(Object.class).size() + " plugins loaded");

//Unload your plugins before exiting
pl.destruct(); //this calls OnUnload on all plugins and removes them from the PluginLoaders list
```

### JitPack

Gradle
```groovy
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
dependencies {
    implementation 'com.gitlab.itwookie:WookiePluginLoader:Version'
}
```