package com.itwookie.pluginloader.exceptions;

public class PluginNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PluginNotFoundException() {
		super("Could not find PluginContainer for Plugin - Maybe the plugin was not propery loaded");
	}
}
