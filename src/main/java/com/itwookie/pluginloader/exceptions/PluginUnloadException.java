package com.itwookie.pluginloader.exceptions;

import com.itwookie.pluginloader.PluginContainer;

public class PluginUnloadException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private final PluginContainer plugin;

	public PluginUnloadException(PluginContainer p, Throwable parent) {
		super("Failed to unload plugin " + p.getName() + " [" + p.getOrigin().toString() + "]", parent);
		this.plugin = p;
	}

	public PluginContainer getPlugin() {
		return plugin;
	}
}
