package com.itwookie.pluginloader.exceptions;

import java.io.IOException;
import java.nio.file.Path;

public class ContainerScanException extends IOException {
	private static final long serialVersionUID = 1L;

	public ContainerScanException(Class<?> clazz, Path origin, String message) {
		super("Unable to construct PluginContainer for " + clazz.getSimpleName() + " [" + origin.toString() + "]: " + message);
	}

	public ContainerScanException(Class<?> clazz, Path origin, String message, Throwable parent) {
		super("Unable to construct PluginContainer for " + clazz.getSimpleName() + " [" + origin.toString() + "]: " + message, parent);
	}
}
