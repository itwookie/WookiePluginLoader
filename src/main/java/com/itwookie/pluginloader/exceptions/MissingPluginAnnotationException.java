package com.itwookie.pluginloader.exceptions;

import java.nio.file.Path;

public class MissingPluginAnnotationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MissingPluginAnnotationException(Path c) {
		super("The following archive did not contain a Plugin: " + c.toString());
	}
}
