package com.itwookie.pluginloader.exceptions;

import com.itwookie.pluginloader.PluginContainer;

public class DependencyResolutionException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DependencyResolutionException(PluginContainer p, String missingDependencyString) {
		super("The plugin " + p.getName() + " is missing "+missingDependencyString);
	}
}
