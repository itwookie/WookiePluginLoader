package com.itwookie.pluginloader.exceptions;

import com.itwookie.pluginloader.PluginContainer;

public class PluginInstantiationException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PluginInstantiationException(PluginContainer p, Throwable parent) {
		super("Failed to instantiate plugin " + p.getName() + " [" + p.getOrigin().toString() + "]", parent);
	}
}
