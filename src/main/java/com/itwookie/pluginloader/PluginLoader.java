package com.itwookie.pluginloader;

import com.itwookie.pluginloader.exceptions.*;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public final class PluginLoader {
	
	private final List<PluginContainer> plugins = new LinkedList<>();
	static final LocalClassLoader loader = new LocalClassLoader(new URL[0], PluginLoader.class.getClassLoader());
	
	public PluginLoader() {
		
	}
	
	/** Scan a plugin JAR, that may contain one or more plugins.<br>
	 * A plugin is defined as class being annotated with a WookiePluginAnnotation and special functions annotated with WookiePluginFunction.<br>
	 * It is highly recommended to at least have a @WookiePluginFunction(Type.onUnload) in order to be able to tell when your plugin get's unloaded and you're supposed to close all resources. 
	 * The JAR file will be scanned for plugin classes automatically. <br>
	 * A plugin has the following lifecycle:<br>
	 * -> Getting create via public default constructor (the plugin was discovered and is being wrapped in the PluginContainer, to make this fast we recommend this to be empty  we want to continue)<br>
	 * -> The onPrepare method is called (The plugin container is completed, scanning for plugins continues, will be run in it's own thread, work within your plugin)<br>
	 * -> Dependency Resolution scanning takes place (All Jars and directories should be scanned, if a dependency is missing execution might halt)<br>
	 * -> The onLoaded method is called (All plugins are there, you may now receive your PluginContainer and work with other plugins, this is synchronous!)<br>
	 * -> Do your work (The application does it's thing and might interact with you, really up to the application)<br>
	 * -> The onUnload method is called (Free any resources you still have opened and prepare for your plugin to be removed from the PluginLoader)<br>
	 * -> Waiting for GC to pick up the stubbed plugin object<br>
	 * If a plugin fails it's public constructor the exception will carry out and halt execution of scanJar, for this reason it's recommended to use the onPrepare function for sensitive operations.<br>
	 * Plugins that fail plugin functions will print stacktrace but not interrupt execution.<br>
	 * Every plugin that successfully prepares will be added to a result list of successfully initialized plugins. If a plugin fails the onPrepare call it will be stubbed, onUnload will <b>NOT</b> be called on this plugin, as it should be removed or fixed immediately!
	 * @param file The jar file to load plugins from
	 * @param ignoreEmptyContainers if this is false, throws an exception if a jar does not declare a plugin
	 * @return Plugins that were successfully loaded
	 * @throws MissingPluginAnnotationException if ignoreUnannotated is false and the Jar-file contains Classes that are not plugins
	 * @throws PluginInstantiationException if the plugin class could not be instantiated or the init lifecycle function failed
	 * @throws ContainerScanException lifecycle functions did not have the correct signature or other values where invalid
	 * @throws IOException on I/O errors
	 */
	public Collection<PluginContainer> scanJar(Path file, boolean ignoreEmptyContainers) throws MissingPluginAnnotationException, PluginInstantiationException, IOException {
		if (!Files.isRegularFile(file) || !file.getFileName().toString().endsWith(".jar"))
			throw new IllegalArgumentException("Specified path is not a jar file");
		//add jar to classloader
		try {
			loader.addURL(file.toUri().toURL());
		} catch (Exception ignore) {}
	    
	    //now that the jar is known we can scan through it similar to a zip archive and search for .class files
	    List<PluginContainer> discovered = new LinkedList<>();
		try (JarFile jar = new JarFile(file.toFile())) {
			Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				String name = entry.getName();
				if (name.endsWith(".class")) {
					name = name.substring(0, name.lastIndexOf('.')).replace('/', '.');
					Class<?> cls;
					try {
						cls = loader.loadClass(name);
					} catch (ClassNotFoundException e) {
						continue; //this was obviously not a class, but had the same extension
					}

					if (!cls.isAnnotationPresent(WookiePluginAnnotation.class)) continue;

					PluginContainer npc = new PluginContainer(this, cls, file);
					discovered.add(npc);

					npc.init();
				}
			}
		}
		if (discovered.isEmpty() && !ignoreEmptyContainers) throw new MissingPluginAnnotationException(file);

	    return discovered;
	}
	
	/** Tries to scan all plugins in the specified folder.<br>
	 * All plugins that successfully initialize will be appended to a list that wraps the plugins in PluginContainers.<br>
	 * For more informations on PluginContainers see scanJar
	 * @param pluginDir the directory that is supposed to be searched for plugins
	 * @throws IllegalArgumentException if pluginDir does not exist or is not a directory
	 * @return List of PluginContainers found in the directory
	 */
	public Collection<PluginContainer> scanDirectory(Path pluginDir) throws IOException {
		List<PluginContainer> scanned = new LinkedList<>();
		
		if (!Files.isDirectory(pluginDir))
			throw new IllegalArgumentException("Please specify an existing directory to scan for plugins");
		
		for (Path f : Files.newDirectoryStream(pluginDir))
			if (Files.isRegularFile(f) && f.getFileName().toString().endsWith(".jar"))
				try {
					scanned.addAll(scanJar(f, true));
				} catch (MissingPluginAnnotationException e) {
					System.err.println(e.getMessage());
				}
		
		return scanned;
	}

	/** After collecting all plugins that were discovered in one or more locations they can be passed to this function in order to be loaded.<br>
	 * Before the onLoad is called the dependencies for every plugin are checked. <br>
	 * Generally missing dependencies should be taken care of, but execution may continue if the application insists on manually checking depndencies<br>
	 * After dependency resolution each plugin gets loaded in a non-specific order. Any exception while loading will result in a exception being thrown.
	 * @param discovered a collection of scanned plugins that shall be loaded
	 * @param ignoreMissingDependencies set to suppress exception whenever dependencies are missing, will still print a short message
	 * @param ignoreFaultyPlugins set to suppress exceptions whenever a plugins fails to execute onLoad, will still print the stacktrace
	 * @throws DependencyResolutionException if dependencies are missing
	 * @throws PluginLoadException if the load-call on a plugin threw a exception
	 */
	public void loadPlugins(Collection<PluginContainer> discovered, boolean ignoreMissingDependencies, boolean ignoreFaultyPlugins) throws DependencyResolutionException, PluginLoadException {
		//Resolve dependencies
		for (PluginContainer plc : discovered) {
			for (PluginDependency dep : plc.getDependencies()) {
				if (!dep.isPresent(discovered)) {
					if (!ignoreMissingDependencies)
						throw new DependencyResolutionException(plc, dep.toString());
					else
						System.err.println(plc.getName() + " is missing the dependency " + dep.toString());
				}
			}
		}

		//Adding the containers first to let plugins search for dependency plugins while onLoaded
		//when the onPluginLoaded is called it should be able to find itself in here
		plugins.addAll(discovered);

		for (PluginContainer plc : discovered) {
			try {
				plc.load();
			} catch (PluginLoadException ex) {
				plugins.remove(ex.getPlugin());
				discovered.remove(ex.getPlugin());
				if (ignoreFaultyPlugins) ex.printStackTrace();
				else throw ex;
			}
		}
	}
	
	/**
	 * Unload all plugins found by this PluginLoader.<br>
	 * It will call onPluginUnloaded and catch any error in order to ensure all plugins are at least tried to be unloaded.<br>
	 * NOTE: this does not guaranty that the plugin actually releases resources or even get's garbage collected.<br>
	 * A bad plugin could still hold references to your application and by this prevent being GC-ed or keep resourced occupied in other ways
	 * This will catch all exceptions and print to System.err
	 */
	public void destruct() {
		destruct(Throwable::printStackTrace);
	}
	/**
	 * Unload all plugins found by this PluginLoader.<br>
	 * It will call onPluginUnloaded and catch any error in order to ensure all plugins are at least tried to be unloaded.<br>
	 * NOTE: this does not guaranty that the plugin actually releases resources or even get's garbage collected.<br>
	 * A bad plugin could still hold references to your application and by this prevent being GC-ed or keep resourced occupied in other ways
	 * This will catch all exceptions
	 * @param exception the exception handler for plugins that fail to unload
	 */
	public void destruct(Consumer<PluginUnloadException> exception) {
		for (PluginContainer plc : plugins)
			try {
				plc.unload();
			} catch (PluginUnloadException e) {
				exception.accept(e);
			}
		plugins.clear();
	}

	/**
	 * Find a certain class by name
	 * @param name the plugin to search
	 * @return empty if no plugin has the specified name
	 */
	public Optional<PluginContainer> getPluginByName(String name) {
		for (PluginContainer plc : plugins)
			if (plc.getName().equalsIgnoreCase(name))
				return Optional.of(plc);
		return Optional.empty();
	}

	/**
	 * Find all plugins that have the specified class as superclass.
	 * This can be useful if you supply plugin archetypes / interfaces that can be implemented.
	 * @param cls the super class to look for
	 * @return all found plugin containers
	 */
	public Collection<PluginContainer> getPluginsBySuperclass(Class<?> cls) {
		List<PluginContainer> plc = new LinkedList<>();
		for (PluginContainer p : plugins)
			if (cls.isAssignableFrom(p.getPluginType()))
				plc.add(p);
		return plc;
	}
	
	/** Get More information about a plugin by the plugin instance.<br>
	 * <b>Never store a PluginContainer in a Plugin instance longer than absolutely necessary!</b><br>
	 * If no PluginContainer exists for this plugin a PluginNotFoundException is thrown */
	public PluginContainer getPluginContainerFor(Object plugin) {
		for (PluginContainer plc : plugins)
			if (plc.getPlugin().equals(plugin))
				return plc;
		throw new PluginNotFoundException();
	}

	/**
	 * Unloads a plugin by container
	 * @param plugin the plugin to unload
	 * @throws PluginNotFoundException if the container is not registered aka the plugin already unloaded
	 */
	public void unloadPlugin(PluginContainer plugin) {
		if (plugin == null) throw new NullPointerException("Can't unload NULL plugin");
		if (!plugins.contains(plugin)) throw new PluginNotFoundException();
		try {
			plugin.unload();
		} catch (Exception e) {
			e.printStackTrace();
		}
		plugins.remove(plugin);
	}

	/**
	 * Registers a shutdown hook to the runtime, trying to unload all plugins.
	 * No guarantees are made to the Unload callback actually being triggered.
	 * The Shutdown hook is no-throw, but will dump all stack-traces.
	 */
	public void unloadOnShutdown() {
		Runtime.getRuntime().addShutdownHook(new Thread(this::destruct));
	}
}
