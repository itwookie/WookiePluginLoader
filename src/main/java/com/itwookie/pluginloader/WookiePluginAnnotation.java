package com.itwookie.pluginloader;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) //Make annotation visible at runtime (why would you not?)
@Target(ElementType.TYPE) 			//Mark as class annotation (METHOD or FIELD possible as well)

public @interface WookiePluginAnnotation {
	/**
	 * For consistency the name is very restrictive an only allows lower case a-z, 0-9 and underscores.
	 * Start with a-z and do not end the name with underscores
	 */
	String Name();
	String Description() default "";
	String Author() default "Unknown";
	String Version() default "1";
	int Build() default 1;
	/** Required dependencies. <br>
	 * Format is "Name" or "Name@BuildMin" or "Name@BuildMin..BuildMax".<br>
	 * e.g.: myFancyDependency@3..5<br>
	 * if there are multiple plugins providing a similar functionality you
	 * can require only one by using || as &lt;or&gt; separator
	 */
	String[] Depends() default {};
}
