package com.itwookie.pluginloader;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) //Make annotation visible at runtime (why would you not?)
@Target(ElementType.METHOD) 		//Mark as method annotation (TYPE or FIELD possible as well)

public @interface WookiePluginFunction {
	enum Type {
		/** Call this function as soon as the plugin instance is created.<br>
		 * This function is supposed to prepare all information that do not rely on other plugins. */
		onPrepare,
		/** Call this function as soon as all plugins are in the registry.<br>
		 * This function should be able to access other plugin containers.<br>
		 * <b>Do not rely on any order of plugins loading and do not keep references to another plugin.</b><br>
		 * In order to retrieve the plugin loader add a PluginContainer argument to the function. */
		onLoaded,
		/** Call this function as it gets regularly removed from the plugin registry.<br>
		 * This function wont be called if your plugin crashes, it's ment to allow you to free resources. */
		onUnload 
	}

	Type value();
}
