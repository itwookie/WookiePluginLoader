package com.itwookie.pluginloader;

import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;

public final class LocalClassLoader extends URLClassLoader {

    public LocalClassLoader(URL[] urls, ClassLoader parent) {
        super(assertLocal(urls), parent);
    }

    public LocalClassLoader(URL[] urls) {
        super(assertLocal(urls));
    }

    public LocalClassLoader(URL[] urls, ClassLoader parent, URLStreamHandlerFactory factory) {
        super(assertLocal(urls), parent, factory);
    }

//    public LocalClassLoader(String name, URL[] urls, ClassLoader parent) {
//        super(name, assertLocal(urls), parent);
//    }
//
//    public LocalClassLoader(String name, URL[] urls, ClassLoader parent, URLStreamHandlerFactory factory) {
//        super(name, assertLocal(urls), parent, factory);
//    }

    @Override
    final protected void addURL(URL url) {
        super.addURL(assertLocal(url));
    }

    private static URL assertLocal(URL url) {
        if (!url.getProtocol().equalsIgnoreCase("file"))
            throw new SecurityException("No remote classes are allowed");
        return url;
    }

    private static URL[] assertLocal(URL[] urls) {
        for (URL url : urls) assertLocal(url);
        return urls;
    }
}
