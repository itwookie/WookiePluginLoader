package com.itwookie.pluginloader;

import com.itwookie.pluginloader.exceptions.ContainerScanException;
import com.itwookie.pluginloader.exceptions.PluginInstantiationException;
import com.itwookie.pluginloader.exceptions.PluginLoadException;
import com.itwookie.pluginloader.exceptions.PluginUnloadException;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public final class PluginContainer {
	private Object plugin;
	private final WookiePluginAnnotation notes;
	private final PluginLoader myLoader;
	private final Path origin;
	private final Class<?> clazz; //useful?
	private Method init=null, load=null, unload=null;
	private final Set<PluginDependency> dependencies;
	private static final Predicate<String> nameValidation = Pattern.compile("^[a-z]([a-z0-9_]*[a-z0-9])?$").asPredicate();

	PluginContainer(PluginLoader link, Class<?> clazz, Path origin) throws ContainerScanException {
		notes = clazz.getDeclaredAnnotation(WookiePluginAnnotation.class); //important to get this first as it's used in exceptions
		if (!nameValidation.test(notes.Name())) throw new ContainerScanException(clazz, origin, "Invalid plugin name "+notes.Name());
		this.origin=origin;
		this.clazz=clazz;
		this.myLoader=link;

		this.dependencies = new HashSet<>();
		for (String dependency : notes.Depends()) {
			this.dependencies.add(new PluginDependency(dependency));
		}

		for (Method m : clazz.getDeclaredMethods())
			if (m.isAnnotationPresent(WookiePluginFunction.class)) {
				WookiePluginFunction an = m.getDeclaredAnnotation(WookiePluginFunction.class);
				if (an.value().equals(WookiePluginFunction.Type.onPrepare)) {
					if ( m.getParameterCount() > 0 ) //we would not know what to put as arguments
						throw new ContainerScanException(clazz, origin, "The plugin function annotated with onPrepare may not have argument!");
					init = m;
				} else if (an.value().equals(WookiePluginFunction.Type.onLoaded)) {
					//throw error if ...
					if ( m.getParameterCount() > 1 ||	// more than one argument is expected
						( m.getParameterCount() == 1 && !PluginContainer.class.isAssignableFrom(m.getParameterTypes()[0]) ) // or the only argument is not a PluginLoader
						)
						throw new ContainerScanException(clazz, origin, "The plugin function annotated with onLoaded may have zero or one PluginContainer argument!");
					// so we can give the plugin an instance to the plugin loader if they need it as soon as it makes sense to use it
					// for example to access dependent plugins or load dependency jars (SECURITY RISK, but quite handy)
					load = m;
				} else if (an.value().equals(WookiePluginFunction.Type.onUnload)) {
					if ( m.getParameterCount() > 0 ) //we would not know what to put as arguments
						throw new ContainerScanException(clazz, origin, "The plugin function annotated with onUnload may not have argument!");
					unload = m;
				}
			}
	}
	
	void init() throws PluginInstantiationException {
		try {
			this.plugin=this.clazz.getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			throw new PluginInstantiationException(this, e);
		}

		if (init==null) return;
		try {
			//we can't invoke a method if it's not accessible
			init.setAccessible(true);
			init.invoke(plugin);
		} catch (Throwable t) {
			throw new PluginInstantiationException(this, t);
		}
	}
	void load() throws PluginLoadException {
		if (load==null) return;
		try {
			//we can't invoke a method if it's not accessible, but we don't really wanna change the value
			load.setAccessible(true);
			if (load.getParameterCount() == 1)
				load.invoke(plugin, this);
			else
				load.invoke(plugin);
		} catch (Throwable t) {
			throw new PluginLoadException(this, t);
		}
	}
	void unload() throws PluginUnloadException {
		if (unload==null) return;
		try {
			//we can't invoke a method if it's not accessible, but we don't really wanna change the value
			unload.setAccessible(true);
			unload.invoke(plugin);
		} catch (Throwable t) {
			throw new PluginUnloadException(this, t);
		}
	}
	
	public String getName() {
		return notes.Name();
	}
	
	public String getDescription() {
		return notes.Description();
	}
	
	public String getAuthor() {
		return notes.Author();
	}
	
	public String getVersion() {
		return notes.Version();
	}
	
	public int getBuild() {
		return notes.Build();
	}
	
	public Object getPlugin() {
		return plugin;
	}
	
	public Class<?> getPluginType() {
		return clazz;
	}
	
	public Path getOrigin() {
		return origin;
	}
	
	public PluginLoader getPluginLoader() {
		return myLoader;
	}

	/** entries in this list are ANDed, the DependencySpecs in each PluginDependency are ORed
	 * @return a list of required dependencies. */
	public Set<PluginDependency> getDependencies() {
		return new HashSet<>(dependencies);
	}

	public InputStream getResource(String name) {
		return PluginLoader.loader.getResourceAsStream(name);
	}
}
