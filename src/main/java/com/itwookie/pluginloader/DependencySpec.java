package com.itwookie.pluginloader;

import java.util.Objects;

final class DependencySpec {
    /** @throws IllegalArgumentException if the version part of the dependency string is malformed */
    DependencySpec(String singleDependency) {
        singleDependency = singleDependency.trim();
        int atAt = singleDependency.indexOf('@');
        if (atAt <= 0) {
            name = singleDependency;
            minBuild = Integer.MIN_VALUE;
            maxBuild = Integer.MAX_VALUE;
        } else {
            name = singleDependency.substring(0, atAt);
            String tmp = singleDependency.substring(atAt + 1);
            atAt = tmp.indexOf("..");
            if (atAt < 0) {
                minBuild = Integer.parseInt(tmp);
                maxBuild = Integer.MAX_VALUE;
                format = 1;
            } else {
                minBuild = Integer.parseInt(tmp.substring(0,atAt));
                maxBuild = Integer.parseInt(tmp.substring(atAt+2));
                if (maxBuild < minBuild) throw new IllegalArgumentException("MinVersion and MaxVersion reversed"); //false order
                format = 2;
            }
        }
    }
    private final String name;
    private final int minBuild;
    private final int maxBuild;

    private int format=0;
    @Override
    public String toString() {
        return format == 0
                ? name
                : (format == 1
                    ? String.format("%s build %d or later", name, minBuild)
                    : String.format("%s build %d to %d", name, minBuild, maxBuild));
    }

    public String getName() {
        return name;
    }

    public int getMinBuild() {
        return minBuild;
    }

    public int getMaxBuild() {
        return maxBuild;
    }
    /** recreates a string matching the annotation value */
    public String toSpecification() {
        return format == 0
                ? name
                : (format == 1
                    ? String.format("%s@%d", name, minBuild)
                    : String.format("%s@%d..%d", name, minBuild, maxBuild));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DependencySpec that = (DependencySpec) o;
        return minBuild == that.minBuild &&
                maxBuild == that.maxBuild &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, minBuild, maxBuild);
    }
}
