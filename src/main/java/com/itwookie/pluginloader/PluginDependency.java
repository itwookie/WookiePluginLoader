package com.itwookie.pluginloader;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/** Represent a dependency group where each single dependency fulfills the same purpose
 * that every other single dependency within this group. <br>
 * This means that only one dependency within this group is required for the plugin to
 * work as intended */
public final class PluginDependency {

    private PluginDependency(){}
    private Set<DependencySpec> requiresOne;
    private boolean missingDeps = true;
    PluginDependency(String definition) {
        requiresOne = Arrays.stream(definition.split("\\|\\|")) // splitting by || or operator
                .map(DependencySpec::new)
                .collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return String.join(" or ",
                requiresOne.stream()
                        .map(DependencySpec::toString)
                        .collect(Collectors.toSet()));
    }

    boolean isPresent(Collection<PluginContainer> plugins) {
        for (DependencySpec dep : requiresOne) {
            if (plugins.stream().anyMatch(x->
                    x.getName().equalsIgnoreCase(dep.getName())&&
                    x.getBuild()>=dep.getMinBuild()&&
                    x.getBuild()<=dep.getMaxBuild())) {
                missingDeps = false;
                return true;
            }
        }
        return false;
    }

    /** this method returns all plugins specs for this dependency. this method is for analytic purposes.
     * @return a set of plugin specifications where one is required for this plugin */
    public Collection<DependencySpec> getValidSpecs() {
        return new HashSet<>(requiresOne);
    }

    /** if missing dependencies are ignored by the loader you can check each plugin with this method */
    public boolean isMissingDeps() {
        return missingDeps;
    }
}
